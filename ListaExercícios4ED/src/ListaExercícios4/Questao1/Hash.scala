package ListaExercícios4.Questao1

import scala.collection.mutable.ListBuffer

class ExtendibleHashTable {
  
    var bucket:Array[Bucket] = null
	  var bits_significativos:Int = 0
	  var size_bucket: Int = 0
	  
	  //Nome:createHash
    //Descricao: Cria Tabela Hash
    def createHash(size:Int) = {
        size_bucket = size
        bits_significativos = 1
        bucket = new Array[Bucket](1)
        bucket(0) = new Bucket
        bucket(0).createBucket(size_bucket)
    }
    
    //Nome: indexCalculate
    //Descricao: Calcula indice
    def indexCalculate(value:Int): Int = {
      var mask:Int = 0
      var masked_n:Int = 0
      for(i <- 1 to bits_significativos){
        mask = 1 << bits_significativos - i
        masked_n += value & mask;
      }
      masked_n
    }
    
    //Nome: divideBucket
    //Descricao: Divide os buckets
    def divideBucket(index:Int)={
      var newBucket:Array[Bucket] = new Array[Bucket](bucket.length+1)
      
      for(i <- 0 until bucket.length)
        newBucket(i) = bucket(i)
      
      newBucket(index+1) = new Bucket
      newBucket(index+1).createBucket(size_bucket)
      newBucket(index).reference = bits_significativos
      newBucket(index+1).reference = bits_significativos
      
      for(i <- 0 until size_bucket){
        var valor = bucket(index).valuesBucket(i)
        if(indexCalculate(valor) != index){
          var newIndex = indexCalculate(valor)
          newBucket(newIndex).addBucket(valor)
          newBucket(index).removeBucket(valor)
        }
      }
      
		  bucket = newBucket
    }
    
    //Nome:divideHash
    //Descricao: Divide a Hash
    def divideHash()= {
      bits_significativos+=1
      var newBucket:Array[Bucket] = new Array[Bucket](Math.pow(2, bits_significativos).toInt)
      var size_old = bucket.length
      
      for(i <- 0 until size_old)
        newBucket(i) = bucket(i)
      
      bucket = new Array[Bucket](newBucket.length)
      
      for(i <- 0 until bucket.length){
        bucket(i) = new Bucket
        bucket(i).createBucket(size_bucket)
        bucket(i).reference = bits_significativos
      }
      
      for(i <- 0 until size_old){
        for(j <- 0 until size_bucket){
          if(newBucket(i).addBucket(j)!= -1)
            addHash(newBucket(i).valuesBucket(j))
        }
      }      
    }
      
    //Nome:addHash
    //Descricao: Esse metodo adiciona um valor a Hash
    def addHash(value:Int):Boolean ={
        var index:Int = indexCalculate(value)
        if(index%2 == 0){
          if(bucket(index).reference < bits_significativos){
            if(!bucket(index).addBucket(value)){
              divideBucket(index)
              addHash(value)
              return true
            }  
          }
          else{
            if(!bucket(index).addBucket(value)){
              divideHash()
              addHash(value)
              return true
            }
          }
        }
        else{
          if(bucket(index-1).reference < bits_significativos){
            if(!bucket(index-1).addBucket(value)){
              divideBucket(index)
              addHash(value)
              return true
            }  
          }
          else{
            if(!bucket(index).addBucket(value)){
              divideHash()
              addHash(value)
              return true
            }
          }
        }
        return false
    }

    //Nome:printHash
    //Descricao: Imprime Hash
    def printHash()={
      for(i <- 0 until bucket.length){
        print("Bucket " + i + " = ")
        bucket(i).printBucket()
        println()
      }
    }
    
    //Nome:searchHash
    //Descricao: Busca valor na Hash
    def searchHash(value:Int):(Int,Option[Int])={
      var index = indexCalculate(value)
      (index,bucket(index).searchBucket(value))
    }
    
    //Nome:removeHash
    //Descricao: Remove valor da Hash
    def removeHash(value:Int)={
      var index = indexCalculate(value)
      bucket(index).removeBucket(value)
    }
    
    //Nome:freeHash
    //Descricao: Libera Hash
    def freeHash()={
      for(i <- 0 until bucket.length){
        bucket(i).freeBucket()
      }
      bucket = null
    }
 
}