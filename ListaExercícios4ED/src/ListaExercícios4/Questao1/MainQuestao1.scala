package ListaExercícios4.Questao1

object MainQuestao1 {
    def main(args: Array[String]) {
         
      
      var hash:ExtendibleHashTable = new ExtendibleHashTable
      
      //item1: Criando uma hash com buckets de tamanho 4
      hash.createHash(4)
      
      //Item 2: Insere 16 valores na hash
      for(i <- 0 until 16){
        hash.addHash(i)
      }
      
      hash.printHash()
		
		//Item 3: Busca um valor na hash
		var value = hash.searchHash(5)
		System.out.println(value); // tupla (Bucket, posicao no Bucket)
	
		//Item 4: Remove valores da hash
    hash.removeHash(2)
    hash.removeHash(4)
    hash.removeHash(6)
   
    hash.printHash()
    
		//Item 5: Libera hash
		hash.freeHash()
     
    }
}
