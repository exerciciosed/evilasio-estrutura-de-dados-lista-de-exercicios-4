package ListaExercícios4.Questao1

class Bucket {
  var valuesBucket:Array[Int] = null
  
  //Nome:CreateBucket
  //Descricao: Cria bucket
  def createBucket(tam:Int) = {
    valuesBucket = new Array[Int](tam)
    valuesBucket = valuesBucket.map{_-1} // transforma todos os valores em -1
  }
  
  //Nome:addBucket
  //Descricao: Esse metodo adiciona um valor no bucket
  def addBucket(value:Int):Boolean ={
    var count:Int = 0
    while(count < valuesBucket.length){
      if(valuesBucket(count) == -1){
          valuesBucket(count) = value
          return true
      }
      count+=1
    }
    return false
  }
  
  //Nome: printBucket
  //Descricao: Imprime valores do bucket
  def printBucket()={
    valuesBucket.filter(_ != -1 )foreach(v =>print(v + " ")) 
  }
	
  //Nome: searchBucket
  //Descricao: Busca valor no buncket e retorna seu indice
  def searchBucket(value:Int) : Option[Int] = {
      valuesBucket.indexWhere( v => v == value) match {
	      case i if i > -1 => Some(i)
	      case other => None		
    	}
	}

  //Nome:removeBucket
  //Descricao: Esse metodo remove um valor do bucket
  def removeBucket(value:Int) = {
      searchBucket(value) match {
	      case Some(i) => valuesBucket(i) = -1
	      case None =>
      } 
  }

  //Nome: freeBucket
  //Descricao: Esse metodo libera o bucket
  def freeBucket() = {
    valuesBucket = null
  }
  
  //Nome: usedBucket
  //Descricao: retorna numero de espacos utilizados no bucket
  def usedBucket():Int = {
    var count:Int = 0
    valuesBucket.filter(_ != -1 )foreach(v => count+=1)
    count
  }

}