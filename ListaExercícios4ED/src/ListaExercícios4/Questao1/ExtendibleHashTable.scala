package ListaExercícios4.Questao1

import scala.collection.mutable.ListBuffer

class ExtendibleHashTable {
  
    var bucket:Array[Bucket] = null
	  var bits_significativos:Int = 0
	  var size_bucket: Int = 0
	  
	  //Nome:createHash
    //Descricao: Cria Tabela Hash
    def createHash(size:Int) = {
        size_bucket = size
        bits_significativos = 1
        bucket = new Array[Bucket](Math.pow(2, bits_significativos).toInt)
        for(i <- 0 until bucket.length){ 
          bucket(i) = new Bucket
          bucket(i).createBucket(size_bucket)
        }
    }
    
    //Nome: indexCalculate
    //Descricao: Calcula indice
    def indexCalculate(value:Int): Int = {
      var mask:Int = 0
      var masked_n:Int = 0
      for(i <- 1 to bits_significativos){
        mask = 1 << bits_significativos - i
        masked_n += value & mask;
      }
      masked_n
    }
    
    //Nome: indexCalculate
    //Descricao: Calcula indice
    def indexCalculate(value:Int,bitssiginificativos:Int): Int = {
      var mask:Int = 0
      var masked_n:Int = 0
      for(i <- 1 to bitssiginificativos){
        mask = 1 << bitssiginificativos - i
        masked_n += value & mask;
      }
      masked_n
    }
    
    //Nome: createNewBucket
    //Descricao: Cria novos buckets e os associa a tabela atual
    def createNewBucket(index:Int)={
      var b:Bucket = bucket(index)
      var flag:Boolean = false
      for(i <- 0 until bucket.length){
        if(bucket(i).equals(b) && i!= index){
            bucket(i) = new Bucket
            bucket(i).createBucket(size_bucket)
            flag = true
        }
      
      }

      if(flag){
        bucket(index) = new Bucket
        bucket(index).createBucket(size_bucket)
        
        for(i <- 0 until b.valuesBucket.length)
          addHash(b.valuesBucket(i))
      }
      else
        increaseHash(index)
    }
    
    //Nome:increaseHash
    //Descricao: Aumenta a Hash
    def increaseHash(index:Int)= {
      bits_significativos+=1
      var newBucket:Array[Bucket] = new Array[Bucket](Math.pow(2, bits_significativos).toInt)
      var size_old:Int = bucket.length
      
      //Cria hash temporaria
      for(i <- 0 until bucket.length)
        newBucket(i) = bucket(i)
      for(i <- bucket.length until newBucket.length){
        newBucket(i) = bucket(indexCalculate(i,bits_significativos-1))
      }
        
      //Extende a hash
      bucket = new Array[Bucket](newBucket.length)
      
      for(i <- 0 until bucket.length){
          bucket(i) = new Bucket
          bucket(i).createBucket(size_bucket)        
      }

      //Associa indices da hash aos buckets correspondentes
      for(i <- 0 until bucket.length){
        if(i%2!=index%2){
            bucket(i) = newBucket(i)
        }
        else{
          for(j <- 0 until size_bucket){
            if (i < size_old) 
              addHash(newBucket(i).valuesBucket(j))  
          }
        } 
       }     
    }
      
    //Nome:addHash
    //Descricao: Esse metodo adiciona um valor a Hash
    def addHash(value:Int):Boolean ={
        var index:Int = indexCalculate(value)
          if(!bucket(index).addBucket(value)){
              createNewBucket(index)
              addHash(value)
              return true
            }  
        
        return false
    }

    //Nome:printHash
    //Descricao: Imprime Hash
    def printHash()={
      for(i <- 0 until bucket.length){
         if(!(i >= Math.pow(2,bits_significativos - 1) && 
             bucket(i) == bucket(indexCalculate(i,bits_significativos-1)))){
              print("Bucket " + i + " = ")
              bucket(i).printBucket()
              println()
         }
      }
    }
    
    //Nome:searchHash
    //Descricao: Busca valor na Hash
    def searchHash(value:Int):(Int,Option[Int])={
      var index = indexCalculate(value)
      (index,bucket(index).searchBucket(value))
    }
    
    //Nome:removeHash
    //Descricao: Remove valor da Hash
    def removeHash(value:Int)={
      var index = indexCalculate(value)
      bucket(index).removeBucket(value)
    }
    
    //Nome:freeHash
    //Descricao: Libera Hash
    def freeHash()={
      for(i <- 0 until bucket.length){
        bucket(i).freeBucket()
      }
      bucket = null
    }
    
}